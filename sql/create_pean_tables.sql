CREATE TABLE users
(
  id integer NOT NULL,
  name character varying(128) NOT NULL,
  email character varying(128) NOT NULL,
  user_name character varying(128) NOT NULL UNIQUE,
  hashed_password character varying(128) NOT NULL,
  salt integer NOT NULL,
  CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;

CREATE TABLE articles
(
  id integer NOT NULL,
  title character varying(128) NOT NULL,
  content character varying(1024),
  user_id integer NOT NULL,
  CONSTRAINT articles_pk PRIMARY KEY (Id),
  CONSTRAINT articles_2_users_fkey FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE articles
  OWNER TO postgres;

CREATE INDEX fki_articles_2_products_fkey
  ON articles
  USING btree
  (user_id);
