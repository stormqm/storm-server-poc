-- Database: storm
-- DROP DATABASE storm;
CREATE DATABASE storm
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'English_United States.1252'
       LC_CTYPE = 'English_United States.1252'
       CONNECTION LIMIT = -1;

	   
-- Table: products
-- DROP TABLE products;

CREATE TABLE products
(
  id integer NOT NULL,
  name character varying(500) NOT NULL,
  description character varying(1024),
  CONSTRAINT products_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE products
  OWNER TO postgres;

  
-- Table: releases
-- DROP TABLE releases;

CREATE TABLE releases
(
  id integer NOT NULL,
  name character varying(256) NOT NULL,
  description character varying(1024),
  start_date date NOT NULL,
  end_date date NOT NULL,
  product integer NOT NULL,
  CONSTRAINT releases_pk PRIMARY KEY (Id),
  CONSTRAINT releases_2_products_fkey FOREIGN KEY (product)
      REFERENCES products (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE releases
  OWNER TO postgres;

-- Index: fki_releases_2_products_fkey

-- DROP INDEX fki_releases_2_products_fkey;

CREATE INDEX fki_releases_2_products_fkey
  ON releases
  USING btree
  (product);

/*  
INSERT INTO products(
            id, name, description)
    VALUES (1, 'Storm', null);
INSERT INTO products(
            id, name, description)
    VALUES (2, 'AGM', null);

INSERT INTO releases(
            id, name, description, start_date, end_date, product)
    VALUES (1, 'MS1', 'Milestone 1', '2013-09-01', '2013-09-30', 1);
INSERT INTO releases(
            id, name, description, start_date, end_date, product)
    VALUES (2, 'MS2', 'Milestone 2', '2013-10-01', '2013-10-31', 1);
INSERT INTO releases(
            id, name, description, start_date, end_date, product)
    VALUES (3, 'MS1', 'Milestone 1', '2013-09-01', '2013-09-30', 2);
*/